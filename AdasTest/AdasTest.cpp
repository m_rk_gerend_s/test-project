#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "bmp.h"
#include "pooling.h"
#include "convolution.h"
#include "fc.h"

using namespace std;

vector<vector<double>> loadWeights(char* path)
{
	vector<vector<double>> weights;

	ifstream in(path);
	if (in)
	{
		int width, height;
		double tmp;

		in >> width;
		in >> height;

		for (int i = 0; i < width; ++i)
		{
			vector<double> column;
			for (int j = 0; j < height; ++j)
			{
				in >> tmp;
				column.push_back(tmp);
			}
			weights.push_back(column);
		}
	}

	return weights;
}

void saveWeights(char* path, vector<vector<double>> weights)
{
	ofstream out(path);

	out << weights.size() << " " << weights.size() << " ";

	for (int i = 0; i < (int)weights.size(); ++i)
	{
		for (int j = 0; j < (int)weights.size(); ++j)
		{
			out << weights[i][j] << " ";
		}
	}
}

double test(char* path, vector<vector<double>> ConvWeights, double convBias, vector<vector<double>> FCWeights, double FCBias)
{
	auto image = bmp(path).getImage();

	auto convResult = convolution::forward(image, 3, ConvWeights, convBias);
	auto poolingResult = pooling::forward(convResult, 2);

	return fc::forward(poolingResult, FCWeights, FCBias);
}

void train(int epochs, double learningSpeed, char* path, vector<vector<double>>& ConvWeights, double convBias, vector<vector<double>>& FCWeights, double FCBias)
{
	while (epochs--)
	{
		auto image = bmp(path).getImage();

		auto forwardConv = convolution::forward(image, 3, ConvWeights, convBias);
		auto forwardPooling = pooling::forward(forwardConv, 2);

		auto forwardFC = fc::forward(forwardPooling, FCWeights, FCBias);

		auto backFC = fc::backward(forwardFC, FCWeights, FCBias);
		// adjust fc weights
		for (int i = 0; i < (int)backFC.size(); ++i)
		{
			for (int j = 0; j < (int)backFC.size(); ++j)
			{
				FCWeights[i][j] += (backFC[i][j] - forwardConv[i][j]) * learningSpeed;
			}
		}

		auto backPooling = pooling::backward(backFC, 2);

		auto backConv = convolution::backward(backPooling, ConvWeights, 3);
		// adjust conv weights
		for (int i = 0; i < (int)backConv.size(); ++i)
		{
			for (int j = 0; j < (int)backConv.size(); ++j)
			{
				ConvWeights[i][j] += (backConv[i][j] - (image[i][j].r + image[i][j].g + image[i][j].b)) * learningSpeed;
			}
		}
	}
}

int main()
{
	auto ConvWeights = loadWeights("convWeights");
	auto FCWeights = loadWeights("FCWeights");

	train(100, 0.003, "1.bmp", ConvWeights, 1.2, FCWeights, 1.2);

	if (test("1.bmp", ConvWeights, 1, FCWeights, 1) > 0.5)
		cout << "I know this!" << endl;
	else
		cout << "I don't know this." << endl;

	saveWeights("convWeights", ConvWeights);
	saveWeights("FCWeights", FCWeights);

    return 0;
}