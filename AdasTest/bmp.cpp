#include "bmp.h"

bmp::bmp(char* fileName)
{
	valid = true;
	std::ifstream in(fileName, std::ios_base::in | std::ios_base::binary);
	if (!in)
	{
		valid = false;
		std::cerr << "Unable to open file\n";
	}
	in >> std::hex;

	in.seekg(0, in.end);
	int length = in.tellg();
	in.seekg(0, in.beg);

	unsigned char* file = new unsigned char[length];
	char a;
	for (int i = 0; i < length && in.get(a); ++i)
		file[i] = (unsigned char)a;

	if (file[0] != 'B' || file[1] != 'M')
		std::cerr << "Not a BMP file.\n";

	if (file[28] != 24)
		std::cerr << "Not 24bpp.\n";

	if (file[30] != 0)
		std::cerr << "Compressed file.\n";

	int offset = file[10];
	int width = file[18];
	int height = file[22];

	int rowSize = int(floor((file[28] * width + 31.) / 32)) * 4;
	int imageSize = rowSize * height;

	pixelData = new unsigned short[imageSize];
	for (int i = 0; i < imageSize; ++i)
	{
		pixelData[i] = (unsigned short)file[i + offset];
	}

	delete[] file;
}

bmp::~bmp()
{
	delete[] pixelData;
}

rgb bmp::getPixel(unsigned short x, unsigned short y)
{
	rgb ret;
	if (x < 52 && y < 52 && x >= 0 && y >= 0)
	{
		y = 52 - 1 - y;
		ret = rgb(pixelData[156 * y + 3 * x + 2], pixelData[156 * y + 3 * x + 1], pixelData[156 * y + 3 * x ]);
	}

	return ret;
}

std::vector<std::vector<rgb>> bmp::getImage()
{
	std::vector<std::vector<rgb>> image;

	for (unsigned short i = 0; i < 52; ++i)
	{
		std::vector<rgb> column;
		for (unsigned short j = 0; j < 52; ++j)
		{
			column.push_back(getPixel(i, j));
		}
		image.push_back(column);
	}

	return image;
}