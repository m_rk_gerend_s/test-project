#ifndef BMP_H
#define BMP_H

#include <iostream>
#include <fstream>
#include <vector>

struct rgb
{
	unsigned short r, g, b;

	rgb() : r(256), g(256), b(256) {}
	rgb(unsigned short R, unsigned short G, unsigned short B) : r(R), g(G), b(B) {}
};

class bmp
{
private:
	unsigned short* pixelData;
	bool valid;

public:
	bmp(char* fileName);
	~bmp();

	rgb getPixel(unsigned short x, unsigned short y);
	std::vector<std::vector<rgb>> getImage();
};

#endif



