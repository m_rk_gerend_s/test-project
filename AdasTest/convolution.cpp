#include "convolution.h"

std::vector<std::vector<double>> convolution::forward(std::vector<std::vector<rgb>> image, unsigned short dimension, std::vector<std::vector<double>> weights, double bias)
{
	std::vector<std::vector<double>> image_ret;

	int wightDimension = ((((int)image.size()) - dimension) / 1 + 1);
	if (wightDimension == weights.size())
	{
		// kernel center
		for (int filterCenterX = dimension / 2; filterCenterX + (dimension / 2) < (int)image.size(); ++filterCenterX)
		{
			std::vector<double> column;
			for (int filterCenterY = dimension / 2; filterCenterY + (dimension / 2) < (int)image.size(); ++filterCenterY)
			{
				// pixel in the kernel area
				double feature = 0.;
				for (int valueX = filterCenterX - (dimension / 2); valueX <= filterCenterX + (dimension / 2); ++valueX)
				{
					for (int valueY = filterCenterY - (dimension / 2); valueY <= filterCenterY + (dimension / 2); ++valueY)
					{
						if (valueX < 0 || valueY < 0 || valueX >= weights.size() || valueY >= weights.size())
							continue;

						feature += (image[valueX][valueY].r + image[valueX][valueY].g + image[valueX][valueY].b) * weights[valueX][valueY];
					}
				}
				column.push_back(tanhl(feature + bias));
			}
			image_ret.push_back(column);
		}
	}

	return image_ret;
}

std::vector<std::vector<double>> convolution::backward(std::vector<std::vector<double>> image, std::vector<std::vector<double>> weights, unsigned short dimension)
{
	for (int i = 0; i < (int)image.size(); ++i)
	{
		for (int j = 0; j < (int)image.size(); ++j)
		{
			image[i][j] = atanhl(image[i][j]);
		}
	}

	auto ret_image = image;

	// kernel center
	for (int toX = 0; toX < (int)ret_image.size(); ++toX)
	{
		for (int toY = 0; toY < (int)ret_image.size(); ++toY)
		{
			// pixel in the kernel area
			double avarage = 0;
			int count = 0;
			for (int fromX = toX - (dimension / 2); fromX <= toX + (dimension / 2); ++fromX)
			{
				for (int fromY = toY - (dimension / 2); fromY <= toY + (dimension / 2); ++fromY)
				{
					if (fromX < 0 || fromY < 0 || fromX >= image.size() || fromY >= image.size())
						continue;

					avarage += image[fromX][fromY] / weights[fromX][fromY];
					++count;
				}
			}
			ret_image[toX][toY] = avarage / count;
		}
	}

	return image;
}