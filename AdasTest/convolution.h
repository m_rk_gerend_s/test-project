#ifndef CONVOLUTION_H
#define CONVOLUTION_H

#include <vector>
#include <algorithm>

#include "bmp.h"

class convolution
{
public:
	static std::vector<std::vector<double>> forward(std::vector<std::vector<rgb>> image, unsigned short dimension, std::vector<std::vector<double>> weights, double bias);
	static std::vector<std::vector<double>> backward(std::vector<std::vector<double>> image, std::vector<std::vector<double>> weights, unsigned short dimension);
};

#endif