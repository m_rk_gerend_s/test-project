#include "fc.h"

double fc::forward(std::vector<std::vector<double>> features, std::vector<std::vector<double>> weights, double bias)
{
	double output = 0;
	for (int i = 0; i < (int)features.size(); ++i)
	{
		for (int j = 0; j < (int)features.size(); ++j)
		{
			output += features[i][j] * weights[i][j];
		}
	}

	return tanhl(output + bias);
}

std::vector<std::vector<double>> fc::backward(double output, std::vector<std::vector<double>> weights, double bias)
{
	double totalVal = atanhl(output) - bias;
	double totalWeights = 0;

	for (auto column : weights)
	{
		for (auto elem : column)
		{
			totalWeights += elem;
		}
	}

	auto features = weights;
	for (int i = 0; i < (int)weights.size(); ++i)
	{
		for (int j = 0; j < (int)weights.size(); ++j)
		{
			features[i][j] = totalVal * totalWeights / weights[i][j];
		}
	}

	return features;
}