#ifndef FC_H
#define FC_H

#include <vector>

class fc
{
public:
	static double forward(std::vector<std::vector<double>> features, std::vector<std::vector<double>> weights, double bias);
	static std::vector<std::vector<double>> backward(double output, std::vector<std::vector<double>> weights, double bias);
};

#endif