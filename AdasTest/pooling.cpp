#include "pooling.h"

std::vector<std::vector<double>> pooling::forward(std::vector<std::vector<double>> image, unsigned short dimension)
{
	std::vector<std::vector<double>> image_ret;

	if (((image.size() - dimension) / dimension + 1) % 10 == 0)
	{
		for (int i = 0; i < (int)image.size(); i += dimension)
		{
			std::vector<double> column;
			for (int j = 0; j < (int)image.size(); j += dimension)
			{
				double max = std::max(std::max(std::max(image[i][j], image[i + 1][j]), image[i][j + 1]), image[i + 1][j + 1]);

				column.push_back(max);
			}
			image_ret.push_back(column);
		}
	}

	return image_ret;
}

std::vector<std::vector<double>> pooling::backward(std::vector<std::vector<double>> image, unsigned short dimension)
{
	std::vector<std::vector<double>> image_ret;

	for (int i = 0; i < (int)image.size(); ++i)
	{
		std::vector<double> column;
		for (int j = 0; j < (int)image.size(); ++j)
		{
			// push the same value again to fill the kernel area
			for (int c = 0; c < (int)dimension; ++c)
				column.push_back(image[i][j]);
		}
		// inserting the same column
		for (int c = 0; c < (int)dimension; ++c)
			image_ret.push_back(column);
	}

	return image_ret;
}