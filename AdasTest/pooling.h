#ifndef POOLING_H
#define POOLING_H

#include <vector>
#include <algorithm>

class pooling
{
public:
	static std::vector<std::vector<double>> forward(std::vector<std::vector<double>> image, unsigned short dimension);
	static std::vector<std::vector<double>> backward(std::vector<std::vector<double>> image, unsigned short dimension);
};

#endif